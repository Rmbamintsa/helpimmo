import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LoginService } from './services/login.service';
import { CommService } from './services/comm.service';
import { BnNgIdleService } from 'bn-ng-idle'; // import bn-ng-idle service
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreerUsersComponent } from './admin/utilisateurs/creer-users/creer-users.component';
import { UsersAdminComponent } from './admin/utilisateurs/users-admin/users-admin.component';
import { LoginComponent } from './login/login.component';
import { HttpModule } from '@angular/http';
import { ToastaModule } from 'ngx-toasta';
import { Toast } from './module/toast';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { MatSelectModule } from '@angular/material/select';
import { RouterModule, Routes } from '@angular/router';
import { Administration } from './guard/Administration';
import { Authentification } from './guard/Authentification';
import {NgxPaginationModule} from 'ngx-pagination';
import { UtilisateurService } from './services/utilisateur.service';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptor } from './services/request.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SiteComponent } from './admin/parametragesite/site/site.component';
import { ParametragesiteComponent } from './admin/parametragesite/parametragesite.component';
import { ListviewSiteComponent } from './module/listview-site/listview-site.component';
import { VignetteComponent } from './module/vignette/vignette.component';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { CheckboxUtilisateursComponent } from 'src/app/admin/Utilisateurs/checkbox-utilisateurs/checkbox-utilisateurs.component';

const appRoutes: Routes = [
  //{ path: '', component: AppComponent },
  { path: 'login', component: LoginComponent },
  //{ path: 'admin/users', component: UsersAdminComponent, canActivate: [Administration, Authentification] },
  { path: 'admin/users', component: UsersAdminComponent},
  //{ path: 'admin/creerusers', component: CreerUsersComponent, canActivate: [Administration, Authentification] },
  { path: 'admin/creerusers', component: CreerUsersComponent},
  { path: 'admin/site', component: SiteComponent, canActivate: [Administration, Authentification] },
  { path: 'admin/parametrersite', component: ParametragesiteComponent, canActivate: [Administration, Authentification] },
];

@NgModule({
  declarations: [
    AppComponent,
    CreerUsersComponent,
    UsersAdminComponent,
    LoginComponent,
    SiteComponent,
    ParametragesiteComponent,
    ListviewSiteComponent,
    VignetteComponent,
    CheckboxUtilisateursComponent
  ],
  entryComponents: [
    SiteComponent,
    ParametragesiteComponent,
    ListviewSiteComponent,
    CheckboxUtilisateursComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    NgxPaginationModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxSmartModalModule.forRoot(),
    ToastaModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger',
      cancelButtonType: 'default'
    })
  ],
  providers: [LoginService,
              CommService,
              BnNgIdleService,
              Toast,
              Administration,
              Authentification,
              UtilisateurService,
              { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },],
  bootstrap: [AppComponent]
})
export class AppModule { }
