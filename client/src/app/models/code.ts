import {Site} from './site';

export class Code{
    id: number;
    site : Site;
    designation: String;
    actif: number;
}
