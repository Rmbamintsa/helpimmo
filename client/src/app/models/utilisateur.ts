import {Site} from './site';

export class Utilisateur {
    id: string;
    nom: string;
    prenom: string;
    mail: string;
    roles: string[];
    sites: Site[];
    selectedSitesId: number[];
    selectedSitesNom: string[];
    isCompt: number;
    isAdmin: number;
    isCom: number;
    actif: number;

}
