import { Utilisateur } from './utilisateur';
import { Code } from './code';

export class Site {
    id: number;
    designation: string;
    pums: Utilisateur;
    codes: Code[];
    constructor() {
        this.pums = new Utilisateur();
    }
}
