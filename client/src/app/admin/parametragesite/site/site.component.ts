import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Code } from '../../../models/code';
import { Toast } from '../../../module/toast';
import { CodeService } from '../../../services/code.service';
import { Site } from '../../../models/site';
import { SiteService } from '../../../services/site.service';
//import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.css']
})

export class SiteComponent {


  @Input()
  designation: String;

  @Input()
  site: Site;

  @Output()
  codeUpdate: EventEmitter<Site> = new EventEmitter();

  @Output()
  siteUpdate: EventEmitter<Site[]> = new EventEmitter();

  @Input()
  sites: Site[];

  codes: Code[];

  constructor(private codeService: CodeService, private siteService: SiteService, private toast: Toast) { }

  rafraichirListeDesCodes() {
    this.codeService.listerCode().subscribe(
      res => {
        this.codes = res;

        this.codeService.sendMessage(this.codes);
        this.rafraichirListeDesSite();
      }
    );
  }

  rafraichirListeDesSite() {
    this.siteService.recupererSite().subscribe(
      res => {
        this.sites = res;
        const i = 0;

        this.siteUpdate.emit(this.sites);
        for (const site of this.sites) {
          if (site.id === this.site.id) {
            this.site = site;
            this.codeUpdate.emit(site);
          }
        }
        this.siteService.sendMessage(this.sites);
      }
    );
  }

  onSubmit() {
    this.siteService.obtenirSiteParDesignation(this.site.designation).subscribe(
      res => {
        const siteDesignation = res;

        let index = this.designation.lastIndexOf(' ');
        while (index > 0) {
          this.designation = this.designation.substring(0, (index));
          index = this.designation.lastIndexOf(' ');
        }

        this.codeService.creerCode(this.designation, siteDesignation.id).subscribe(
          (result: String) => {
            if (result === 'Code créé avec succès !') {
              this.toast.toastOptions.msg = 'Le code ' + this.designation + ' a été créé !';
              this.toast.toastaService.success(this.toast.toastOptions);
              this.rafraichirListeDesSite();
            } else {
              this.toast.toastOptions.msg = 'Le code ' + this.designation + ' a été créé !';
              this.toast.toastaService.success(this.toast.toastOptions);
              this.rafraichirListeDesSite();
            }
          },
          error => {
            this.toast.toastOptions.msg = 'Le code ' + this.designation + ' existe déjà !';
            this.toast.toastaService.error(this.toast.toastOptions);
          }
        );
      }
    );
  }
}