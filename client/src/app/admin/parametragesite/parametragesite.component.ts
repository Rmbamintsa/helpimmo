import { Component, OnInit, Input } from '@angular/core';
import { Site } from '../../models/site';
import { SiteService } from '../../services/site.service';
import { CommService } from '../../services/comm.service';
import { Code } from '../../models/code';
import { CodeService } from '../../services/code.service';
import { Toast } from '../../module/toast';

@Component({
    selector: 'app-parametragesite',
    templateUrl: './parametragesite.component.html',
    styleUrls: ['./parametragesite.component.css']
})

export class ParametragesiteComponent implements OnInit {
    sites: Site[];
    codes: Code[];
    page = 'Paramétrage Sites';
    siteArray = new Array();
    sit: Site;

    hidden = false;

    @Input()
    designation: string;

    constructor(private siteService: SiteService, private commService: CommService,
        private codeService: CodeService, private toast: Toast) {
        this.commService.clearMessage();
        this.commService.sendMessage(this.page);
        this.codeService.getMessage().subscribe(codes => { this.codes = codes; });
    }

    ngOnInit() {
        this.codeService.listerCode().subscribe(
            codes => {
                this.codes = codes;
                this.hidden = true;
            }
        );
        this.siteService.recupererSite().subscribe(
            sites => {
                this.sites = sites;
                this.siteArray = this.sites;
            }
        );
    }

    rafraichirListeDesSites() {
        this.siteService.recupererSite().subscribe(
            res => {
                if (res === null) {
                } else {
                    this.sites = res;
                    this.siteService.sendMessage(this.sites);
                    this.siteArray = this.sites;
                }
            }
        );
    }

    onSubmit() {
        this.siteService.creerSite(this.designation.trim()).subscribe(
            (res: String) => {
                if (res === 'Site créé avec succès.') {
                    this.toast.toastOptions.msg = 'Le site ' + this.designation + ' a été créé avec succès !';
                    this.toast.toastaService.success(this.toast.toastOptions);
                    this.rafraichirListeDesSites();
                }
            },
            error => {
                this.toast.toastOptions.msg = 'Le site ' + this.designation + ' existe déjà';
                this.toast.toastaService.error(this.toast.toastOptions);
            }
        );
    }

    confirmSuppression(id: number) {
        this.siteService.supprimerSite(id).subscribe(
            (res: String) => {
                if (res === 'Site supprimé avec succès.') {
                    this.toast.toastOptions.msg = 'Site supprimé avec succès !';
                    this.toast.toastaService.success(this.toast.toastOptions);
                    this.rafraichirListeDesSites();
                }
            },
            error => {
                this.toast.toastOptions.msg = "Le site nexiste pas. !";
                this.toast.toastaService.error(this.toast.toastOptions);
                console.log(error);
            }
        );
    }
}
