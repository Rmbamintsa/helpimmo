import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxUtilisateursComponent } from './checkbox-utilisateurs.component';

describe('CheckboxUtilisateursComponent', () => {
  let component: CheckboxUtilisateursComponent;
  let fixture: ComponentFixture<CheckboxUtilisateursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckboxUtilisateursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxUtilisateursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
