import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DefaultEditor } from 'ng2-smart-table';

@Component({
  selector: 'app-checkbox-utilisateurs',
  templateUrl: './checkbox-utilisateurs.component.html',
  styleUrls: ['./checkbox-utilisateurs.component.css']
})
export class CheckboxUtilisateursComponent extends DefaultEditor implements OnInit {

  @Input() rowData: any;
  @Output() checkboxEmitter: EventEmitter<boolean> = new EventEmitter();


  name: string;
  checked: boolean;

  constructor() {
    super();
   }

  ngOnInit() {
    const roles = this.rowData.roles;
    for (const role in roles) {
      if (roles[role].nom === this.name) {
        this.checked = true;
      }

    }
  }


  setRole(role: string) {
    this.name = role;
  }

  changeBoolean() {
    this.checked = !this.checked;
    if (this.checked) {
      this.checkboxEmitter.emit(this.checked);
      console.log(this.name);
    }
  }
}
