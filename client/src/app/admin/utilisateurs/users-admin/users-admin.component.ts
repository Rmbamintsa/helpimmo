import { Component, OnInit } from '@angular/core';
import { Utilisateur } from './../../../models/utilisateur';
import { Site } from './../../../models/site';
import { CommService } from '../../../services/comm.service';
import { UtilisateurService } from '../../../services/utilisateur.service';
import { LoginService } from '../../../services/login.service';
import { SiteService } from '../../../services/site.service';
import { Toast } from '../../../module/toast';


@Component({
  selector: 'app-users-admin',
  templateUrl: './users-admin.component.html',
  styleUrls: ['./users-admin.component.css']
})

export class UsersAdminComponent implements OnInit {

  currentPage = 1;
  itemsPerPage = 10;
  editCurrentRow: number = null;

  allUtilisateurs: Utilisateur[];
  utilisateursPourTab: Utilisateur[];
  allSites: Site[];
  tempEdit = new Utilisateur();

  searchTextNom = '';
  searchTextPrenom = '';
  searchTextMail = '';
  searchTextSite = '';

  hidden = false;
  estTrie: number[];
  estTrieRole: number[];
  utilisateurFiltre: Utilisateur[];
  mailDuplique = false;

  page = 'Paramétrage Utilisateurs';


  constructor(
    private utilisateurService: UtilisateurService,
    private loginService: LoginService,
    private siteService: SiteService,
    private commService: CommService,
    private toast: Toast) { }


  ngOnInit() {
    this.estTrie = [];
    this.estTrie.push(-1);
    this.estTrie.push(-1);
    this.estTrie.push(-1);

    this.estTrieRole = [];
    this.estTrieRole.push(1);
    this.estTrieRole.push(1);
    this.estTrieRole.push(1);

    this.recupererToutLesUtilisateurs();
    this.siteService.recupererSite().subscribe(sites => this.allSites = sites);
    this.commService.clearMessage();
    this.commService.sendMessage(this.page);
  }

  recupererToutLesUtilisateurs() {
    this.utilisateurService.recupererUtilisateurs().subscribe(utilisateurs => {
      this.allUtilisateurs = utilisateurs;

      this.allUtilisateurs.map(utilisateur => {
        utilisateur.selectedSitesId = [];
        utilisateur.selectedSitesNom = [];
        utilisateur.isAdmin = 0;
        utilisateur.isCompt = 0;
        utilisateur.isCom = 0;

        utilisateur.sites.map(site => {
          utilisateur.selectedSitesId[utilisateur.selectedSitesId.length] = site.id;
          utilisateur.selectedSitesNom[utilisateur.selectedSitesNom.length] = site.designation;
        });
        utilisateur.roles.map((role: any) => {

          if (role.id === 1) {
            utilisateur.isAdmin = 1;
          }
          if (role.id === 2) {
            utilisateur.isCompt = 1;
          }
          if (role.id === 3) {
            utilisateur.isCom = 1;
          }
        });
      });
      this.recupererUtilisateursPourTableau();

      this.hidden = true;
    });
  }

  recupererUtilisateursPourTableau() {
    this.utilisateursPourTab = [];
    this.allUtilisateurs.map(utilisateur => {
      if (utilisateur.nom.toString().toLowerCase().includes(this.searchTextNom.toLowerCase())
        && utilisateur.prenom.toString().toLowerCase().includes(this.searchTextPrenom.toLowerCase())
        && utilisateur.mail.toString().toLowerCase().includes(this.searchTextMail.toLowerCase())
        && utilisateur.selectedSitesNom.toString().toLowerCase().includes(this.searchTextSite.toLowerCase())
      ) {
        this.utilisateursPourTab.push(utilisateur);
      }
    });
    this.utilisateurFiltre = this.utilisateursPourTab;
    this.trierRole("");
  }

  adaptDataFromToggle(value) {
    if (value != null) {
      if (value === true) {
        return 1;
      } else if (value === false) {
        return 0;
      } else {
        return value;
      }
    }
  }

  annulerEdition(utilisateur) {
    this.editCurrentRow = null;

    utilisateur.nom = this.tempEdit.nom;
    utilisateur.prenom = this.tempEdit.prenom;
    utilisateur.mail = this.tempEdit.mail;
    utilisateur.isCompt= this.tempEdit.isCompt;
    utilisateur.isAdmin = this.tempEdit.isAdmin;
    utilisateur.isCom = this.tempEdit.isCom;
    utilisateur.selectedSitesId = this.tempEdit.selectedSitesId;
  }

  sauvegarderModifUtilisateur(utilisateur: any) {
    this.editCurrentRow = null;

    utilisateur.roles = [];
    utilisateur.sites = [];

    utilisateur.isCompt = this.adaptDataFromToggle(utilisateur.isCompt);
    utilisateur.isAdmin = this.adaptDataFromToggle(utilisateur.isAdmin);
    utilisateur.isCom = this.adaptDataFromToggle(utilisateur.isCom);

    utilisateur.roles.push('USER');

    if (utilisateur.isAdmin === 1) {
      utilisateur.roles.push('ADMIN');
    }

    if (utilisateur.isCompt === 1) {
      utilisateur.roles.push('COMPT');
    }

    if (utilisateur.isCom === 1) {
      utilisateur.roles.push('COM');
    }

    if (utilisateur.isPum === 0 || utilisateur.isCompt === 0 || utilisateur.isCom === 0) {
      utilisateur.sites = [];
    } else {
      utilisateur.sites = utilisateur.selectedSitesId;
    }

    this.utilisateursPourTab.forEach(
      res => {
        if (res.id != utilisateur.id && res.mail == utilisateur.mail) {
          this.mailDuplique = true;
          alert("cette adresse mail a déjà été affectée à un autre utilisateur !");
          setTimeout(() => { this.recupererToutLesUtilisateurs(); }, 300);
          return;
        }
      }
    );

    if (utilisateur.isCompt === 1 && utilisateur.sites.length === 0) {
      alert("Veuillez affecter un site au Comptable !");
      setTimeout(() => { this.recupererToutLesUtilisateurs(); }, 300);
      return;
    }
    if (utilisateur.isCom === 1 && utilisateur.sites.length === 0) {
      alert("Veuillez affecter un site au Commercial !");
      setTimeout(() => { this.recupererToutLesUtilisateurs(); }, 300);
      return;
    }
    console.log(utilisateur.sites);
    console.log(utilisateur);
    if (utilisateur.isAdmin === 1 && utilisateur.sites.length === 0) {
      alert("Veuillez affecter un site au à l'admin !");
      setTimeout(() => { this.recupererToutLesUtilisateurs(); }, 300);
      return;
    }

    if (utilisateur.isCompt === 1 && utilisateur.isCom === 1) {
      alert("Impossible, un Comptable ne peut pas être Commercial et vice versa !");
      setTimeout(() => { this.recupererToutLesUtilisateurs(); }, 300);
      return;
    } if (utilisateur.isCompt === 1 && utilisateur.isAdmin === 1) {
      alert("Impossible, un Comptable ne peut pas être Admin et vice versa !");
      setTimeout(() => { this.recupererToutLesUtilisateurs(); }, 300);
      return;
    } if (utilisateur.isCom === 1 && utilisateur.isAdmin === 1) {
      alert("Impossible, un Commercial ne peut pas être Admin et vice versa !");
      setTimeout(() => { this.recupererToutLesUtilisateurs(); }, 300);
      return;
    } else if (utilisateur.isCompt === 0 && utilisateur.isCom === 0 && utilisateur.isAdmin === 0) {
      alert("Impossible, un utilisateur doit avoir au moins un role!");
      setTimeout(() => { this.recupererToutLesUtilisateurs(); }, 300);
      return;
    }
    if (utilisateur.nom === "") {
      alert("Veuillez saisir un nom d'utilisateur !");
      setTimeout(() => { this.recupererToutLesUtilisateurs(); }, 300);
      return;
    }
    if (utilisateur.prenom === "") {
      alert("Veuillez saisir un prénom d'utilisateur  !");
      setTimeout(() => { this.recupererToutLesUtilisateurs(); }, 300);
      return;
    }

    if (!this.mailDuplique) {
      if (utilisateur.mail.indexOf('@capgemini.com') > -1) {
        this.utilisateurService.modifierUtilisateur(utilisateur);
      } else {
        alert("Adresse email invalide !");
        setTimeout(() => { this.recupererToutLesUtilisateurs(); }, 300);
      }
    } else {
      if (this.mailDuplique) {
        this.mailDuplique = false;
      }
      this.utilisateursPourTab.forEach(
        res => {
          if (res.id != utilisateur.id && res.mail == utilisateur.mail) {
            this.mailDuplique = true;
          }
        }
      );
      if (!this.mailDuplique) {
        if (utilisateur.mail.indexOf('@capgemini.com') > -1) {
          this.utilisateurService.modifierUtilisateur(utilisateur);
        } else {
          alert("Adresse email invalide !");
          setTimeout(() => { this.recupererToutLesUtilisateurs(); }, 300);
        }
      }
    }

    if (JSON.parse(localStorage.getItem('portalUser')).utilisateur.roles.filter((role) => role.nom === "ADMIN").length == 1 && JSON.parse(localStorage.getItem('portalUser')).utilisateur.id == utilisateur.id && JSON.parse(localStorage.getItem('portalUser')).utilisateur.mail != utilisateur.mail) {
      this.loginService.logOut();
    } else {
      setTimeout(() => { this.recupererToutLesUtilisateurs(); }, 300);
    }
    this.recupererToutLesUtilisateurs();

  }

  onSwitchToggle(value) {
    if (value != null) {
      if (value === 1) {
        value = 0;
      } else {
        value = 1;
      }
    }
  }

  switchEditRow(utilisateur) {
    this.editCurrentRow = utilisateur.id;

    this.tempEdit.prenom = utilisateur.prenom;
    this.tempEdit.nom = utilisateur.nom;
    this.tempEdit.mail = utilisateur.mail;
    this.tempEdit.isCompt = utilisateur.isCompt;
    this.tempEdit.isAdmin = utilisateur.isAdmin;
    this.tempEdit.isCom = utilisateur.isCom;
    this.tempEdit.selectedSitesId = utilisateur.selectedSitesId;
  }

  trier(idColonne) {
    let nomColonne = document.getElementById(idColonne).innerHTML.trim();
    let thisThis = this;

    if (nomColonne == "Nom") {
      this.estTrie[1] = -1;
      if (this.estTrie[0] == -1) this.estTrie[0] = 1;
      else this.estTrie[0] = -1;

      this.utilisateursPourTab.sort(function (a: Utilisateur, b: Utilisateur) {
        if (a.nom > b.nom)
          return 1 * (thisThis.estTrie[0]);
        if (a.nom < b.nom)
          return -1 * (thisThis.estTrie[0]);
        return 0;
      });
    }

    if ((nomColonne.toString() === "Mail") || nomColonne.toString() === "Prénom") {
      this.estTrie[0] = -1;
      if (this.estTrie[1] == -1) this.estTrie[1] = 1;
      else this.estTrie[1] = -1;

      this.utilisateursPourTab.sort(function (a: Utilisateur, b: Utilisateur) {
        if (a.prenom > b.prenom)
          return 1 * thisThis.estTrie[1];
        if (a.prenom < b.prenom)
          return -1 * thisThis.estTrie[1];
        return 0;
      });
    }
  }

  trierRole(role) {
    if (role != "") {
      let nomColonne = document.getElementById(role).innerHTML.trim().toLowerCase();
      if (nomColonne === "compt") {
        this.estTrieRole[0] = this.estTrieRole[0] * (-1);
      }
      if (nomColonne === "com") {
        this.estTrieRole[1] = this.estTrieRole[1] * (-1);
      }
      if (nomColonne === "admin") {
        this.estTrieRole[2] = this.estTrieRole[2] * (-1);
      }
    }

    let utilisateurTrie = [];
    let rechercheUtilisateurs;
    if (this.utilisateurFiltre === []) rechercheUtilisateurs = this.allUtilisateurs;
    else rechercheUtilisateurs = this.utilisateurFiltre;


    if (this.estTrieRole[0] == 1) {
      document.getElementById('compt').className = "selected";
      rechercheUtilisateurs.forEach(utilisateur => {
        if (utilisateur.isCom && !utilisateurTrie.includes(utilisateur)) utilisateurTrie.push(utilisateur);
      });
    }
    else document.getElementById('compt').className = "notSelected";


    if (this.estTrieRole[1] == 1) {
      document.getElementById('com').className = "selected";
      rechercheUtilisateurs.forEach(utilisateur => {
        if (utilisateur.isCom && !utilisateurTrie.includes(utilisateur)) utilisateurTrie.push(utilisateur);
      });
    }
    else document.getElementById('com').className = "notSelected";


    if (this.estTrieRole[2] == 1) {
      document.getElementById('admin').className = "selected";
      rechercheUtilisateurs.forEach(utilisateur => {
        if (utilisateur.isAdmin && !utilisateurTrie.includes(utilisateur)) utilisateurTrie.push(utilisateur);
      });
    }
    else document.getElementById('admin').className = "notSelected";

    this.utilisateursPourTab = utilisateurTrie;
  }

  onDelete(id: number) {
    if (JSON.parse(localStorage.getItem('portalUser')).utilisateur.roles.filter((role) => role.nom === "ADMIN").length == 1 && JSON.parse(localStorage.getItem('portalUser')).utilisateur.id == id) {
      alert("Impossible de supprimer un administrateur");
      return;
    }
    this.utilisateurService.supprimerUtilisateur(id).subscribe(
      res => {
        if (res == null) {
          this.toast.toastOptions.msg = "L'utilisateur n'a pas pu être supprimé !";
          this.toast.toastaService.error(this.toast.toastOptions);
        } else {
          this.toast.toastOptions.msg = "L'utilisateur a été supprimé avec succès !";
          this.toast.toastaService.success(this.toast.toastOptions);
          setTimeout(() => { this.recupererToutLesUtilisateurs(); }, 300);
        }
      });
  }

}
