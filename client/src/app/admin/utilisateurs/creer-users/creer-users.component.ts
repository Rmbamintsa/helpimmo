import { Component, OnInit } from '@angular/core';
import { Utilisateur } from 'src/app/models/utilisateur';
import { Role } from 'src/app/models/role';
import { Site } from 'src/app/models/site';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import { Router } from '@angular/router';
import { CommService } from 'src/app/services/comm.service';
import { SiteService } from 'src/app/services/site.service';
import { Toast } from 'src/app/module/toast';


@Component({
  selector: 'app-creer-users',
  templateUrl: './creer-users.component.html',
  styleUrls: ['./creer-users.component.css']
})


export class CreerUsersComponent implements OnInit {

  utilisateur: Utilisateur = new Utilisateur();
  roles: Role[];
  isAdmin: boolean;
  isCompt: boolean;
  isCom: boolean;
  page = 'Paramétrage Utilisateurs';
  controlCode = true;
  sites: Site[];;
  site: Site[];
  listeUtilisateurs: Array<any> = [];
  mailExiste = false;


  constructor(
    private utilisateurService: UtilisateurService,
    private router: Router,
    private commService: CommService,
    private siteService: SiteService,
    private toast: Toast
  ) {
    this.commService.sendMessage(this.page);
  }


  ngOnInit() {
    this.utilisateurService.listerRoles().subscribe(
      roles => {
        this.roles = roles;
      }
    );

    this.utilisateurService.recupererUtilisateurs().subscribe(
      utilisateurs => {
        this.listeUtilisateurs = utilisateurs;
      }
    );
    // Chargement de la liste des sites
    this.siteService.recupererSite().subscribe(
      sites => {
        this.sites = sites;
      });

  }



  onSubmit() {
    this.utilisateur.roles = [];
    this.utilisateur.sites = [];

    this.utilisateur.roles.push('USER');

    if (this.isAdmin) {
      this.utilisateur.roles.push('ADMIN');
    }
    if (this.isCompt) {
      this.utilisateur.roles.push('COMPT');
    }
    if (this.isCom) {
      this.utilisateur.roles.push('COM');
    }
    this.utilisateur.sites = this.site;

    if (this.utilisateur.mail.indexOf('@') == -1) {
      alert("Adresse email invalide !");
      return;
    }
    if (this.utilisateur.sites === undefined) {
      alert("Veuillez affecter un site à l'utilisateur!");
      return;
    }
    if (this.isCompt && this.isAdmin) {
      alert("Impossible, un comptable ne peut pas être Admin et vice versa !");
      return;
    } if (this.isCompt && this.isCom) {
      alert("Impossible, un comptable ne peut pas être Commercial et vice versa !");
      return;
    } if (this.isAdmin && this.isCom) {
      alert("Impossible, un Admin ne peut pas être Commercial et vice versa !");
      return;
    } else if (!this.isCompt && !this.isAdmin && !this.isCom) {
      alert("Impossible, un utilisateur doit avoir au moins un role!");
      return;
    }

    for (let utilisateur of this.listeUtilisateurs) {
      if (utilisateur.mail === this.utilisateur.mail) {
        this.mailExiste = true;
        break;
      } else {
        this.mailExiste = false;
      }
    }

    if (this.mailExiste) {
      alert("L'adresse mail existe déjà !");
      return;
    }
    this.utilisateurService.creerUtilisateur(this.utilisateur);
    this.utilisateurService.recupererUtilisateurs();
    setTimeout(() => { this.router.navigate(['/admin/users']); }, 2000);
  }


}
