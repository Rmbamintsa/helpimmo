import { Component, Input } from '@angular/core';


export interface LegendItem {
    title: string;
    imageClass: string;
}

export enum ChartType {
    Pie,
    Line,
    Bar
}

@Component({
    selector: 'vignette',
    templateUrl: './vignette.component.html',
    styleUrls: ['./vignette.component.css']
})


export class VignetteComponent {

    @Input() public title: string;
    @Input() public subtitle: string;

    @Input()
    public chartClass: string;

    @Input()
    public chartType: ChartType;

    @Input()
    public chartData: any;

    @Input()
    public chartOptions: any;

    @Input()
    public chartResponsive: any[];

    @Input()
    public footerIconClass: string;

    @Input()
    public legendItems: LegendItem[];

    @Input()
    public withHr: boolean;

    constructor() { }


}
