import { ToastOptions, ToastaService, ToastaConfig } from 'ngx-toasta';
import { Injectable } from '../../../node_modules/@angular/core';

@Injectable()
export class Toast {

    public toastOptions: ToastOptions = {
        title: '',
        msg: 'The message',
        showClose: true,
        timeout: 5000,
        theme: 'bootstrap',
    };

    constructor(public toastaService: ToastaService) {
    }
}
