import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Site } from '../../models/site';
import { Code } from '../../models/code';
import { SiteService } from '../../services/site.service';
import { CodeService } from '../../services/code.service';
import { Toast } from '../../module/toast';
import { ParametragesiteComponent } from '../../admin/parametragesite/parametragesite.component';
@Component({
    selector: 'app-listview-site',
    templateUrl: './listview-site.component.html',
    styleUrls: ['./listview-site.component.css']
})
export class ListviewSiteComponent implements OnInit {

    @Input()
    public site: Site;

    @Output()
    listSite: EventEmitter<Site> = new EventEmitter<Site>();

    codes: Code[];

    constructor(private codeService: CodeService, private toast: Toast, private parametragesiteComponent: ParametragesiteComponent) {
        this.codeService.getMessage().subscribe(codes => { this.codes = codes; });
    }

    ngOnInit() {
    }

    confirmSuppressionCode(id: number) {
        this.codeService.supprimerCode(id).subscribe(
            (res: Code) => {
                if (res != null) {
                    this.toast.toastOptions.msg = 'Code supprimé avec succès !';
                    this.toast.toastaService.success(this.toast.toastOptions);
                    this.parametragesiteComponent.rafraichirListeDesSites();
                }
            },
            error => {
                this.toast.toastOptions.msg = "Le code nexiste pas. !";
                this.toast.toastaService.error(this.toast.toastOptions);
                console.log(error);
            }
        );
    }
}
