import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from '../services/login.service';

@Injectable()
export class Administration implements CanActivate {

  constructor(private router: Router, private loginService : LoginService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.loginService.isAdmin)
      return true;
    this.router.navigate(['/home'], { queryParams: { returnUrl: state.url }});
    return false;
  }
}