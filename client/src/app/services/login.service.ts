import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Login } from '../models/login';
import { map } from 'rxjs/operators';
import { Toast } from '../module/toast';
import { Router } from '@angular/router';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

//@Injectable()
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  baseUrl = environment.baseUrlBack;
  principal: any = null;
  base64Credential: string;
  logged = false;
  isAdmin = false;
  isPum = false;
  isEm = false;
  isCom = false;
  isUser = false;
  username: string;
  nom: string;
  prenom: string;
  initialPrenom: string;

  constructor(public http: Http, private toast: Toast, private router: Router) { }

  public getRole() {
    if (this.principal) {
      for (const authority in this.principal.authorities) {
        if ('ADMIN' === this.principal.authorities[authority].authority) {
          this.isAdmin = true;
        }
        if ('PUM' === this.principal.authorities[authority].authority) {
          this.isPum = true;
        }
        if ('EM' === this.principal.authorities[authority].authority) {
          this.isEm = true;
        }
        if ('COM' === this.principal.authorities[authority].authority) {
          this.isCom = true;
        }
        if ('USER' === this.principal.authorities[authority].authority) {
          this.isUser = true;
        }
      }
    } else {
      this.isAdmin = false;
    }
  }
  public logIn(login: Login)  {
    this.base64Credential = btoa(login.username + ':');
    const headers = new Headers();
    const options = new RequestOptions();

    headers.append('Accept', 'application/json');
    headers.append('Authorization', 'Basic ' + this.base64Credential);
    options.headers = headers;
  		return this.http.get(this.baseUrl + '/login', options)
  		  .pipe(map((response: Response) => {
        if (response.status === 200) {
            console.log("statut 200");
    			  this.principal = response.json().principal;
    			  if (this.principal) {
    				this.logged = true;
    				this.username = login.username;
    				this.nom = this.principal.utilisateur.nom;
    				this.prenom = this.principal.utilisateur.prenom;
    				this.initialPrenom = this.principal.utilisateur.prenom.substring(0, 1) + '. ';
    				localStorage.setItem('portalUser', JSON.stringify(this.principal));
    				localStorage.setItem('login', this.base64Credential);
    				this.isAdmin = false;
    				this.getRole();
    			  this.toast.toastOptions.msg = 'Connexion réussie ! Bienvenue ' + this.prenom + ' !';
    			  this.toast.toastaService.success(this.toast.toastOptions);
    			} else {
    			  this.toast.toastOptions.msg = 'Connexion refusée ! Veuillez ré-essayer.';
    			  this.toast.toastaService.error(this.toast.toastOptions);
    			}

  		  }else if(response == null){
  			  this.toast.toastOptions.msg = 'Connexion refusée ! Veuillez ré-essayer.';
  			  this.toast.toastaService.error(this.toast.toastOptions);
  		  }else{
  			  this.toast.toastOptions.msg = 'Connexion refusée ! Veuillez ré-essayer.';
  			  this.toast.toastaService.error(this.toast.toastOptions);
  		  }
        },
            (error : Error) => {
                this.toast.toastOptions.msg = 'Connexion refusée ! Veuillez ré-essayer.';
                this.toast.toastaService.error(this.toast.toastOptions);
                return Observable.throw(error);
            }))/*.catch((err) => {
                      this.toast.toastOptions.msg = 'Connexion refusée ! Veuillez ré-essayer.';
                      this.toast.toastaService.error(this.toast.toastOptions);
                      return Observable.throw(err.statusText);
                 }
              );*/
  }

  logOut() {
    this.logged = false;
    this.principal = null;
    this.base64Credential = '';
    this.isAdmin = false;
    this.isEm = false;
    this.isPum = false;
    this.isUser = false;
    this.username = null;
    this.nom = null;
    this.prenom = null;
    this.initialPrenom = null;
    localStorage.clear();
    return this.http.post(this.baseUrl + '/logout', {})
      .pipe(map((response: Response) => {
        if (response.status === 200) {
          this.toast.toastOptions.msg = 'Déconnexion réussie !';
          this.toast.toastaService.success(this.toast.toastOptions);
          this.router.navigate(['home']);
        } else {
          this.toast.toastOptions.msg = 'Une erreur est survenue, veuillez ré-essayer.';
          this.toast.toastaService.error(this.toast.toastOptions);
        }
      })).subscribe();
  }
}
