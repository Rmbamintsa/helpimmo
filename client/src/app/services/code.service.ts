import { environment } from '../../environments/environment';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Code } from '../models/code';

@Injectable({
    providedIn: 'root'
})

export class CodeService {

    baseUrl = environment.baseUrlBack;
    codeUrl = "/Code";


    codesSubject = new Subject<any>();
    codes: Code[];

    constructor(private http: HttpClient) { }

    creerCode(designation: String, site: number) {
        const url = '/CreerCode';
        return this.http.post(this.baseUrl + url, { designation: designation, site: site }, { responseType: 'text' as 'text' });
    }

    listerCode() {
        const url = '/ListerCodes';
        return this.http.get<Code[]>(this.baseUrl + url);
    }

    sendMessage(codes: Code[]) {
        this.codesSubject.next(codes);
    }

    clearMessage() {
        this.codesSubject.next();
    }

    getMessage(): Observable<any> {
        return this.codesSubject.asObservable();
    }

    supprimerCode(id: number) {
        const url = "/SupprimerCode";
        return this.http.post(this.baseUrl + url, { id: id });
    }
}
