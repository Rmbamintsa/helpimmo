import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const login = localStorage.getItem('login');
    let authReq;
    if (login) {
      authReq = req.clone({ headers: req.headers.set('Authorization', 'Basic ' + login) });
    } else {
      authReq = req.clone();
    }
    return next.handle(authReq);
  }
}
