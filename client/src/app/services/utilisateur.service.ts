import { environment } from '../../environments/environment';
import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Utilisateur } from '../models/utilisateur';
import { Role } from '../models/role';
import { Toast } from '../module/toast';

@Injectable()
export class UtilisateurService {

    usersSubject = new Subject<any>();
    private utilisateurs = {};
    private getUtilisateurs = [];
    listeRole: Role[];
    baseUrl = environment.baseUrlBack;

    constructor(private client: HttpClient, private toast: Toast) { }

    emitUtilisateursSubject() {
        this.usersSubject.next(this.getUtilisateurs.slice());
    }

    creerUtilisateur(utilisateur: Utilisateur) {
        this.client.post(this.baseUrl + '/creerUtilisateur', utilisateur).subscribe(
            res => {
                if (res === null) {
                    this.toast.toastOptions.msg = 'L\'utilisateur ' + utilisateur.nom + ' ' + utilisateur.prenom + ' n\'a pu être créé. Veuillez recommencer.';
                    this.toast.toastaService.error(this.toast.toastOptions);
                } else {
                    this.toast.toastOptions.msg = 'L\'utilisateur ' + utilisateur.nom + ' ' + utilisateur.prenom + ' a été créé avec succès !';
                    this.toast.toastaService.success(this.toast.toastOptions);
                }
            });

    }

    modifierUtilisateur(utilisateur: Utilisateur) {
        this.client.post(this.baseUrl + '/creerUtilisateur', utilisateur).subscribe(
            res => {
                if (res === null) {
                    this.toast.toastOptions.msg = 'L\'utilisateur ' + utilisateur.nom + ' ' + utilisateur.prenom + ' n\'a pu être modifié. Veuillez recommencer.';
                    this.toast.toastaService.error(this.toast.toastOptions);
                } else {
                    this.toast.toastOptions.msg = 'L\'utilisateur ' + utilisateur.nom + ' ' + utilisateur.prenom + ' a été modifié avec succès !';
                    this.toast.toastaService.success(this.toast.toastOptions);
                }
            });

    }

    supprimerUtilisateur(id: number) {
        return this.client.delete(this.baseUrl + '/supprimerUtilisateur/' + id);
    }

    recupererUtilisateurs() {
        return this.client.get<Utilisateur[]>(this.baseUrl + '/listerUtilisateur');
    }

    listerRoles() {
        const url = '/listerRole';
        return this.client.get<Role[]>(this.baseUrl + url);
    }

    getUtilisateur(id){
        return this.client.get<Utilisateur>(this.baseUrl + "/obtenirUtilisateurParId/"+id);
    }

    public recupererUtilisateursParRole(role: String) {
        const url = '/listerUtilisateurParRole/' + role;
        return this.client.get<Utilisateur[]>(this.baseUrl + url);
    }

    // intercepteur
    sendMessage(utilisateur: Utilisateur[]) {
        this.usersSubject.next(utilisateur);
    }

    clearMessage() {
        this.usersSubject.next();
    }

    getMessage(): Observable<any> {
        return this.usersSubject.asObservable();
    }

    getListeUtilisateurs() {
        return this.getUtilisateurs;
    }
}
