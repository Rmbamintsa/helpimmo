import { environment } from '../../environments/environment';
import { Site } from '../models/site';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class SiteService {

  sitesSubject = new Subject<any>();
  sites: Site[];
  baseUrl = environment.baseUrlBack;
  siteUrl = "/Site";

  constructor(private http: HttpClient) { }

  creerSite(designation: String) {
    const url = '/CreerSite';
    return this.http.post(this.baseUrl + url, { id: null, designation: designation, codes: null }, { responseType: 'text' as 'text' });
  }

  recupererSite() {
    const url = '/ListerSites';
    return this.http.get<Site[]>(this.baseUrl + url);
  }

  obtenirSiteParDesignation(designation: String) {
    const url = '/SiteDesignation/' + designation;
    return this.http.get<Site>(this.baseUrl + url);
  }

  sendMessage(sites: Site[]) {
    this.sitesSubject.next(sites);
  }
  sendeMessage(site: Site) {
    this.sitesSubject.next(site);
  }

  clearMessage() {
    this.sitesSubject.next();
  }

  getMessage(): Observable<any> {
    return this.sitesSubject.asObservable();
  }

  supprimerSite(id: number) {
    const url = `${this.siteUrl}/${id}`;
    return this.http.delete(this.baseUrl + url, { responseType: 'text' as 'text' });
  }
}
