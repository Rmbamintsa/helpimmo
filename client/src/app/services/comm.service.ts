import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class CommService {

    page: string;

    pageSubject = new Subject<string>();
    pageSubjectObs = this.pageSubject.asObservable();

    dataSubject = new Subject<any>();
    dataSubjectObs = this.dataSubject.asObservable();


    getPageObs() {
        return this.pageSubjectObs;
    }

    sendMessage(message: string) {
        this.pageSubject.next(message);
    }

    clearMessage() {
        this.pageSubject.next();
    }

    sendData(donnees: any) {
        this.dataSubject.next(donnees);
    }

    getDataObs() {
        return this.dataSubjectObs;
    }

}
