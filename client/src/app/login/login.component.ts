import { Component, Input } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '../../../node_modules/@angular/router';
import { Login } from '../models/login';
import { NgForm } from '@angular/forms';
import { ViewChild, ElementRef} from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  login: Login = new Login();
  errorMessage: string;
  @Input() logged: boolean;
  @ViewChild('modalConnexion', { static: true }) modal: ElementRef;

  constructor(public loginService: LoginService, public router: Router ) { }

  onLogin(f: NgForm) {
    this.loginService.logIn(this.login)
      .subscribe(data => {
        f.reset();
        this.router.navigate(['home']);
        document.getElementById('modalConnexion').click();
      }
    );

  }


}
