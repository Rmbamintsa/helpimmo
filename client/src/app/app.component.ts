import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Subscription } from '../../node_modules/rxjs';
import { CommService } from './services/comm.service';
import { LoginService } from './services/login.service';
import { VERSION } from '@angular/material';
import { BnNgIdleService } from 'bn-ng-idle'; // import bn-ng-idle service
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent implements OnInit, OnDestroy {

  //version = VERSION;
  page: string;
  titreSubscription: Subscription;


  constructor(public commService: CommService, public loginService: LoginService, private bnIdle: BnNgIdleService, private router: Router) {
    this.titreSubscription = this.commService.getPageObs().subscribe(
      (value) => {
        this.page = value;
      }
    );

    this.bnIdle.startWatching(300).subscribe((res) => {
      if (res) {
        localStorage.clear();
        this.router.navigate(['login']);
        window.location.reload();
      }
    })
  }


  ngOnInit() {
    this.loginService.principal = JSON.parse(localStorage.getItem('portalUser'));
    if (this.loginService.principal) {
      this.loginService.base64Credential = localStorage.getItem('login');
      this.loginService.getRole();
      this.loginService.logged = true;
      this.loginService.username = this.loginService.principal.username;
      this.loginService.nom = this.loginService.principal.utilisateur.nom;
      this.loginService.initialPrenom = this.loginService.principal.utilisateur.prenom.substring(0, 1) + '. ';
    }
  }



  ngOnDestroy() {
    this.titreSubscription.unsubscribe();
  }


}
