LOCK TABLES `t_utilisateur` WRITE;
/*!40000 ALTER TABLE `t_utilisateur` DISABLE KEYS */;
INSERT INTO `t_utilisateur` (`id`, `mail`, `nom`, `prenom`, `login_id`, `actif`) VALUES (1,'admin@capgemini.com','Admin','Admin',1,1);                                    							
/*!40000 ALTER TABLE `t_utilisateur` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `t_login` WRITE;
/*!40000 ALTER TABLE `t_login` DISABLE KEYS */;
INSERT INTO `t_login` (`username`) VALUES ('admin@capgemini.com');                                      							
/*!40000 ALTER TABLE `t_login` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `t_role` WRITE;
/*!40000 ALTER TABLE `t_role` DISABLE KEYS */;
INSERT INTO `t_role` (`id`, `nom`) VALUES (1,'ADMIN'),(2,'COMPT'),(3,'COM'),(4,'USER');
/*!40000 ALTER TABLE `t_role` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `t_utilisateur_roles` WRITE;
/*!40000 ALTER TABLE `t_utilisateur_roles` DISABLE KEYS */;
INSERT INTO `t_utilisateur_roles` (`users_id`, `roles_id`) VALUES (1,1);
/*!40000 ALTER TABLE `t_utilisateur_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

LOCK TABLES `t_site` WRITE;
/*!40000 ALTER TABLE `t_site` DISABLE KEYS */;
INSERT INTO `t_site` (`id`, `designation`) VALUES (1,'Libreville');
/*!40000 ALTER TABLE `t_site` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `t_utilisateur_sites` WRITE;
/*!40000 ALTER TABLE `t_utilisateur_sites` DISABLE KEYS */;
INSERT INTO `t_utilisateur_sites` (`sites_id`, `pums_id`) VALUES (1,1);
/*!40000 ALTER TABLE `t_utilisateur_sites` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `t_code` WRITE;
/*!40000 ALTER TABLE `t_code` DISABLE KEYS */;
INSERT INTO `t_code` (`id`, `actif`,`designation`, `site_id`) VALUES (1,1,'libreville',1);
/*!40000 ALTER TABLE `t_code` ENABLE KEYS */;
UNLOCK TABLES;