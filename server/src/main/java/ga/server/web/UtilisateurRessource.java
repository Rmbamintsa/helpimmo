package ga.server.web;

import ga.server.Entities;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(UtilisateurRessource.PATH)
public class UtilisateurRessource {
    private static final String NAME = Entities.UTILISATEUR;
    static final String PATH = Entities.API_PATH + "/" + NAME + "s";

}
