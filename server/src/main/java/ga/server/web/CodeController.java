package ga.server.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ga.server.domaine.Code;
import ga.server.services.dto.CodeTO;
import ga.server.services.interfaces.ICodeService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CodeController {

	@Autowired
	ICodeService codeService;

	@GetMapping(value = "/ListerCodes")
	public ResponseEntity<List<Code>> listercodes() {

		try {
			return new ResponseEntity<>(codeService.listerCodes(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PostMapping(value = "/CreerCode")
	public ResponseEntity<String> creerCode(@Valid @RequestBody CodeTO codeTo) {

		try {
			if (codeService.creerCode(codeTo)) {
				return new ResponseEntity<>("Code créé avec succès.", HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>("Le code existe déjà en base.", HttpStatus.CONFLICT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping(value = "/Code/{id}")
	public ResponseEntity<Code> recupererCodeParId(@Valid @PathVariable Long id) {

		try {

			return new ResponseEntity<>(codeService.obtenirCodeParId(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PutMapping("/ModifierCode")
	public ResponseEntity<String> modifierCode(@Valid @RequestBody CodeTO codeTo) {

		try {
			if (codeService.modifierCode(codeTo)) {
				return new ResponseEntity<>("Code modifié avec succès.", HttpStatus.ACCEPTED);
			} else {
				return new ResponseEntity<>("La modification a échouée", HttpStatus.CONFLICT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PostMapping(value = "/SupprimerCode")
	public ResponseEntity<Code> supprimerCode(@Valid @RequestBody CodeTO codeTo) {

		try {
			return new ResponseEntity<>(codeService.supprimerCode(codeTo.getId()), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}