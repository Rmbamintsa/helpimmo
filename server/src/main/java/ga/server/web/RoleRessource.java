package ga.server.web;

import ga.server.Entities;
import ga.server.services.RoleService;
import ga.server.services.dto.RoleDTO;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping(RoleRessource.PATH)
public class RoleRessource {
    private static final String NAME = Entities.ROLE;
    static final String PATH = Entities.API_PATH + "/" + NAME + "s";

    @Autowired
    private RoleService service;

    /*@GetMapping("")
    public Collection<RoleDTO> finfAllRoles(){
        return service.retrieveAllRoles();
    }*/

    /*@PostMapping("")
    public ResponseEntity<RoleDTO> create(RoleDTO dtoTocreate) throws Exception {

        return new ResponseEntity<RoleDTO>(service.create(dtoTocreate), HttpStatus.ACCEPTED);
    }*/


    @PutMapping("/{code}")
    public ResponseEntity<RoleDTO> updateRole(@PathVariable String code, @Valid @RequestBody RoleDTO dtoToUpdate){
        //dtoToUpdate.setCode(code != null ? code : Strings.EMPTY);
        Optional<RoleDTO> updatedDto = service.update(dtoToUpdate);
        if(updatedDto.isPresent()){
            return ResponseEntity.ok().body(updatedDto.get());
        }
        return ResponseEntity.notFound().build();

    }


}
