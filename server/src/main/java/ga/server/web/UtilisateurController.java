package ga.server.web;

import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ga.server.config.PortailRmLog;
import ga.server.domaine.Role;
import ga.server.domaine.Site;
import ga.server.domaine.Utilisateur;
import ga.server.services.RoleService;
import ga.server.services.SiteService;
import ga.server.services.UtilisateurService;
import ga.server.services.dto.UtilisateurTO;
import ga.server.services.dto.UtilisateurTransfertObject;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UtilisateurController {

	@Autowired
	private UtilisateurService utilisateurService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private SiteService siteService;
	Logger logger = LoggerFactory.getLogger(UtilisateurController.class);

	@RequestMapping("login")
	public ResponseEntity<Principal> user(Principal principal) {

		logger.info("user logged : {}", principal);
		return new ResponseEntity<>(principal, HttpStatus.OK);

	}

	private static final String CHAMP_UTILISATEUR = "Utilisateur";

	@PostMapping(value = "creerUtilisateur")
	public ResponseEntity<Utilisateur> creerUtilisateur(@Valid @RequestBody UtilisateurTransfertObject user) {

		try {
			PortailRmLog.logCreation(logger, CHAMP_UTILISATEUR);
			Utilisateur utilisateur = new Utilisateur();
			if (user.getId() != null) {
				utilisateur = utilisateurService.recupererUtilisateurParID(Long.parseLong(user.getId()));
			}

			utilisateur.setNom(user.getNom());
			utilisateur.setPrenom(user.getPrenom());
			utilisateur.setMail(user.getMail());
			utilisateur.setActif(1);
			Set<Role> roles = new HashSet<>();
			for (String nomRole : user.getRoles()) {
				roles.add(roleService.roleParNom(nomRole));
			}
			List<String> listeSites = user.getSites();
			Set<Site> sites = new HashSet<>();
			if (user.getSites() != null) {
				for (int i = 0; i < listeSites.size(); i++) {
					sites.add(siteService.obtenirSiteParId(Long.parseLong(listeSites.get(i))));
				}
			}

			return new ResponseEntity<>(utilisateurService.creerUtilisateur(utilisateur, roles, sites),
					HttpStatus.CREATED);
		} catch (Exception e) {
			PortailRmLog.logErreur(logger, "creerUtilisateur()", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping(value = "listerUtilisateur")
	public ResponseEntity<Iterable<Utilisateur>> listerUtilisateur() {

		try {
			PortailRmLog.logListe(logger, CHAMP_UTILISATEUR);
			return new ResponseEntity<>(utilisateurService.listerUtilisateur(), HttpStatus.OK);
		} catch (Exception e) {
			PortailRmLog.logErreur(logger, "listerUtilisateur()", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/*@GetMapping(value = "listerUtilisateurParRole/{role}")
	public ResponseEntity<Iterable<UtilisateurTO>> listerUtilisateurParRole(@PathVariable String role) {

		try {
			PortailRmLog.logListe(logger, "Utilisateurs par role");
			return new ResponseEntity<>(utilisateurService.listerUtilisateurParRole(role), HttpStatus.OK);
		} catch (Exception e) {
			PortailRmLog.logErreur(logger, "listerUtilisateurParRole()", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}*/

	@DeleteMapping(value = "supprimerUtilisateur/{id}")
	public ResponseEntity<Utilisateur> supprimerUtilisateur(@PathVariable String id) {

		try {
			PortailRmLog.logSuppression(logger, CHAMP_UTILISATEUR, Long.parseLong(id));
			return new ResponseEntity<>(utilisateurService.supprimerUtilisateur(id), HttpStatus.OK);
		} catch (Exception e) {
			PortailRmLog.logErreur(logger, "supprimerUtilisateur()", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping(value = "obtenirUtilisateurParId/{id}")
	public ResponseEntity<Utilisateur> obtenirUtilisateurParId(@PathVariable Long id) {

		try {
			PortailRmLog.logListe(logger, CHAMP_UTILISATEUR);
			return new ResponseEntity<>(utilisateurService.obtenirUtilisateurParId(id), HttpStatus.OK);
		} catch (Exception e) {
			PortailRmLog.logErreur(logger, "obtenirUtilisateurParId()", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PutMapping("modifierUtilisateur")
	public ResponseEntity<Utilisateur> modifierUtilisateur(@Valid @RequestBody Utilisateur utilisateur) {

		try {
			PortailRmLog.logModification(logger, CHAMP_UTILISATEUR, utilisateur.getId());
			return new ResponseEntity<>(utilisateurService.modifierUtilisateur(utilisateur), HttpStatus.OK);
		} catch (Exception e) {
			PortailRmLog.logErreur(logger, "modifierUtilisateur()", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
