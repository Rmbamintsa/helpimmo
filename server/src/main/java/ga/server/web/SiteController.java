package ga.server.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ga.server.domaine.Site;
import ga.server.services.dto.SiteTO;
import ga.server.services.interfaces.ICodeService;
import ga.server.services.interfaces.ISiteService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SiteController {

	@Autowired
	ISiteService siteService;
	@Autowired
	ICodeService codeService;

	@GetMapping(value = "/ListerSites")
	public ResponseEntity<List<Site>> listerSites() {

		try {
			codeService.supprimerCodesInactifs(0);
			return new ResponseEntity<>(siteService.listerSites(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PostMapping(value = "/CreerSite")
	public ResponseEntity<String> creerSite(@Valid @RequestBody SiteTO siteTo) {

		try {
			if (siteService.creerSite(siteTo)) {
				return new ResponseEntity<>("Site créé avec succès.", HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>("Le site existe déjà en base.", HttpStatus.CONFLICT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/Site/{id}")
	public ResponseEntity<Site> recupererSiteParId(@Valid @PathVariable Long id) {
		try {
			return new ResponseEntity<>(siteService.obtenirSiteParId(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping(value = "/SiteDesignation/{designation}")
	public ResponseEntity<Site> recupererSiteParDesignation(@Valid @PathVariable String designation) {

		try {
			return new ResponseEntity<>(siteService.obtenirSiteParDesignation(designation), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PutMapping("/ModifierSite")
	public ResponseEntity<String> modifierSite(@Valid @RequestBody SiteTO siteTo) {

		try {
			if (siteService.modifierSite(siteTo)) {
				return new ResponseEntity<>("Site modifié avec succès.", HttpStatus.ACCEPTED);
			} else {
				return new ResponseEntity<>("La modification a échouée", HttpStatus.CONFLICT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@DeleteMapping(value = "/Site/{id}")
	public ResponseEntity<String> supprimerSite(@Valid @PathVariable Long id) {

		try {
			if (siteService.supprimerSite(id)) {
				return new ResponseEntity<>("Site supprimé avec succès.", HttpStatus.ACCEPTED);
			} else {
				return new ResponseEntity<>("Le site n'existe pas.", HttpStatus.CONFLICT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}