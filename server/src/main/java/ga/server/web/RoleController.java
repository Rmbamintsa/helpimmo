package ga.server.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ga.server.config.PortailRmLog;
import ga.server.domaine.Role;
import ga.server.services.RoleService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class RoleController {

	@Autowired
	private RoleService roleService;
	Logger logger = LoggerFactory.getLogger(RoleController.class);

	@GetMapping(value = "/listerRole")
	public ResponseEntity<List<Role>> listerRole() {

		try {
			PortailRmLog.logListe(logger, "Roles");
			return new ResponseEntity<>(roleService.listeRole(), HttpStatus.OK);
		} catch (Exception e) {
			PortailRmLog.logErreur(logger, "listerRoles()", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
