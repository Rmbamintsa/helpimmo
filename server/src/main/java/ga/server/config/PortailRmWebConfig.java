package ga.server.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import ga.server.services.UtilisateurService;

@Configurable
@EnableWebSecurity
public class PortailRmWebConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired UtilisateurService loginUserDetailsService;
	
	private static final String ADMIN = "ADMIN";
	private static final String COMPT = "COMPT";
	private static final String COM = "COM";
	private static final String USER = "USER";
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.userDetailsService(loginUserDetailsService);
		
	}
		
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.cors();
		http.httpBasic();
		http.authorizeRequests()
		.antMatchers("/admin/*").hasAnyAuthority(ADMIN)
		.antMatchers("/compt/*").hasAnyAuthority(ADMIN,COMPT)
		.antMatchers("/com/*").hasAnyAuthority(ADMIN,COM)
		.antMatchers("/user/*").hasAnyAuthority(ADMIN,USER)
		//.antMatchers("/com/*").hasAnyAuthority(ADMIN,PUM,COM)
		.anyRequest().authenticated()	
        .and()
		.logout().permitAll().logoutRequestMatcher(new AntPathRequestMatcher("/logout", "POST")).logoutSuccessUrl("/login?logout");	
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
		http.csrf().disable();
		
	}

}
