package ga.server.config;

import org.slf4j.Logger;

public class PortailRmLog {

	private PortailRmLog() {

	}

	public static void logCreation(Logger logger, String nomClasse) {

		if (logger.isInfoEnabled()) {
			logger.info(String.format("Création d'un objet %s", nomClasse));
		}

	}

	public static void logListe(Logger logger, String nomClasse) {

		if (logger.isInfoEnabled()) {
			logger.info(String.format("Récupération de la liste des %s", nomClasse));
		}

	}

	public static void logModification(Logger logger, String nomClasse, Long id) {

		if (logger.isInfoEnabled()) {
			logger.info(String.format("Modification de l'objet %s ayant pour id %d", nomClasse, id));
		}

	}

	public static void logSuppression(Logger logger, String nomClasse, Long id) {

		if (logger.isInfoEnabled()) {
			logger.info(String.format("Suppression de l'objet %s ayant pour id %d", nomClasse, id));
		}

	}

	public static void logErreur(Logger logger, String nomDeFonction, Exception e) {

		if (logger.isErrorEnabled() || logger.isInfoEnabled()) {
			logger.error(String.format("Une erreur est survenue dans la fonction %s1 : %s2", nomDeFonction, e.getMessage()));
		}

	}
	
}
