package ga.server.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ga.server.domaine.Login;
import ga.server.domaine.Utilisateur;
import ga.server.repository.LoginRepository;

@Service
public class LoginService  {
	@Autowired
	private LoginRepository db;

	public List<Login> listerLogin() {
		List<Login> result = new ArrayList<>();
		for (Login login : db.findAll()) {	
			result.add(login);
		}
		return result;
	}

	public Login chercherParNom(String username) {
		/*List<Login> logins = listerLogin();
		Login login = null;
		for (Login xlogin : logins) {	
			if (xlogin.getUsername().equals(username))
				login = xlogin;
		}
		return login; */
		return db.findLoginByUsername(username);
	}
}
