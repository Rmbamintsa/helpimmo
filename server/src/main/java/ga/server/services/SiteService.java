package ga.server.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ga.server.domaine.Site;
import ga.server.repository.SiteRepository;
import ga.server.services.dto.SiteTO;
import ga.server.services.interfaces.ISiteService;

@Service
public class SiteService implements ISiteService {

	@Autowired private SiteRepository siteRepository;
	@Override
	public boolean creerSite(SiteTO siteTo) {

		if (siteRepository.sitePremierAjout(siteTo.getDesignation())) {
			String designation = siteTo.getDesignation().substring(0, 1).toUpperCase()+siteTo.getDesignation().substring(1).toLowerCase().trim();
			siteTo.setDesignation(designation);
			Site site = new Site(siteTo);
			siteRepository.save(site);
			siteRepository.listerSites();
			return true;
		}
		return false;
		
	}

	@Override
	public Site obtenirSiteParId(Long id) {
		
		return siteRepository.obtenirSiteParId(id);
		
	}

	@Override
	public Site obtenirSiteParDesignation(String designation) {
		
		return siteRepository.obtenirSiteParDesignation(designation);
		
	}

	@Override
	public boolean modifierSite(SiteTO siteTo) {

		Site site;
		if (siteTo.getId() == null) {
			return false;
		}
		else {
			site = obtenirSiteParId(siteTo.getId());
			if (site == null) {
				return false;
			}
		}	
		site.setDesignation(siteTo.getDesignation());
		siteRepository.listerSites();
		return true;
		
	}

	@Override
	public boolean supprimerSite(Long id) {
		
		if (!siteRepository.existsById(id)) {
			return false;
		}
		siteRepository.deleteById(id);
		return true;
		
	}

	@Override
	public List<Site> listerSites() {

		return siteRepository.listerSites();
		
	}

}