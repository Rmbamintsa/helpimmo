package ga.server.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ga.server.domaine.Code;
import ga.server.domaine.Site;
import ga.server.repository.CodeRepository;
import ga.server.repository.SiteRepository;
import ga.server.services.dto.CodeTO;
import ga.server.services.interfaces.ICodeService;

@Service
public class CodeService implements ICodeService {

	@Autowired private CodeRepository codeRepository;
	@Autowired private SiteRepository siteRepository;
	private List<Code> tousLesCodes = null;
	
	@Override
	public boolean creerCode(CodeTO codeTo) {
		
		if (codeRepository.codePremierAjout(codeTo.getDesignation())) {
			Code code = new Code(codeTo);
			if (codeTo.getSite() != null) {
				Site site = siteRepository.obtenirSiteParId(codeTo.getSite());
				code.setSite(site);
				code.setActif(1);
			}
			codeRepository.save(code);
			this.tousLesCodes = codeRepository.listerCodes();
			return true;
		}
		return false;
		
	}

	@Override
	public Code obtenirCodeParId(Long id) {

		return codeRepository.obtenirCodeParId(id);
		
	}

	@Override
	public Code obtenirCodeParDesignation(String designation) {
		
		return codeRepository.obtenirCodeParDesignation(designation);
		
	}

	@Override
	public boolean modifierCode(CodeTO codeTo) {

		Code code;
		if (codeTo.getId() == null) {
			return false;
		}
		else {
			code = obtenirCodeParId(codeTo.getId());
			if (code == null) {
				return false;
			}
		}	
		code.setDesignation(codeTo.getDesignation());
		this.tousLesCodes = codeRepository.listerCodes();
		return true;
		
	}


	@Override
	public List<Code> listerCodes() {
		this.tousLesCodes = codeRepository.listerCodes();
		return this.tousLesCodes;
		
	}

	@Override
	public List<Code> listerCodesParSite(Site site) {

		return codeRepository.listerCodesParSite(site);
		
	}

	@Override
	public Code supprimerCode(Long id) {
		if (!codeRepository.existsById(id)) {
			return null;
		}
		Code code = codeRepository.obtenirCodeParId(id);
		code.setActif(0);
		return codeRepository.save(code);
	}

	@Override
	public void supprimerCodesInactifs(int id) {
		codeRepository.supprimerCodesInactifs(id);
		
	}

}
