package ga.server.services.mapper;

import java.util.Collection;
import java.util.List;

public interface EntityMapper<D,E> {
    E toEntity( D dto);
    D toDto(E entity);

    List<E> toEntity(Collection<D> dtoList);

    List<D> toDto(Collection<E> entityList);
}
