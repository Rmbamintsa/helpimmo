package ga.server.services.mapper;

import java.util.Collection;
import java.util.List;

import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import ga.server.domaine.Role;
import ga.server.services.dto.RoleDTO;

@Mapper
@Component
public class RoleMapper implements EntityMapper<RoleDTO, Role> {

	@Override
	public Role toEntity(RoleDTO dto) {
		Role role = new Role();
		role.setNom(dto.getNom());
		//role.setCode(dto.getCode());
		role.setId(dto.getId());
		return role;
	}

	@Override
	public RoleDTO toDto(Role entity) {
		RoleDTO roleDTO = new RoleDTO();
		roleDTO.setNom(entity.getNom());
		//roleDTO.setCode(entity.getCode());
		roleDTO.setId(entity.getId());
		return roleDTO;
	}

	@Override
	public List<Role> toEntity(Collection<RoleDTO> dtoList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RoleDTO> toDto(Collection<Role> entityList) {
		// TODO Auto-generated method stub
		return null;
	}
}
