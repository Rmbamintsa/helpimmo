package ga.server.services.mapper;

import ga.server.domaine.Utilisateur;
import ga.server.services.dto.UtilisateurDto;
import org.mapstruct.Mapper;

@Mapper
public interface UtilisateurMapper extends EntityMapper<UtilisateurDto, Utilisateur>{

}
