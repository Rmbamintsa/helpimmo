package ga.server.services;

import ga.server.Messages;
import ga.server.domaine.Role;
import ga.server.repository.RoleRepository;
import ga.server.services.dto.RoleDTO;
import ga.server.services.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleMapper roleMapper;

    /*public RoleDTO create(RoleDTO dtoToCreate) throws Exception {
        if(dtoToCreate !=null && dtoToCreate.getCode()!= null){
            Role existingRole = roleRepository.findRoleByCode(dtoToCreate.getCode());
            if(existingRole != null){
                throw new Exception(Messages.NOT_UNIQUE_ROLE_CODE);
            }

        }
        return roleMapper.toDto(roleRepository.save(roleMapper.toEntity(dtoToCreate)));
    }*/

    public Optional<RoleDTO> update(RoleDTO dtoToUpdate){
        Role databaseEntity = roleRepository.getOne(dtoToUpdate.getId());
        if(databaseEntity == null){
            return Optional.empty();
        }

        return  Optional.of(roleMapper.toDto(roleRepository.save(roleMapper.toEntity(dtoToUpdate))));
    }

    /*public void deleteRole(String code) throws Exception {
        Role databaseEntity =roleRepository.findRoleByCode(code);
        if(databaseEntity == null){
            throw new Exception(Messages.NOT_FOUND_ROLE);
        }
        roleRepository.delete(databaseEntity);
    }*/

    /*public Collection<RoleDTO> retrieveAllRoles(){
        return roleMapper.toDto(roleRepository.findAll());
    }*/
    
    public List<Role> listeRole(){
		return (List<Role>) roleRepository.findAll();
	}
	
	public Role recupererRoleAdmin() {
		return roleRepository.recupererRoleAdmin();
	}
	
	public Role recupererRoleCOMPT() {
		return roleRepository.recupererRoleCOMPT();
	}
	
	public Role recupererRoleCOM() {
		return roleRepository.recupererRoleCOM();
	}
	
	public Role recupererRoleUser() {
		return roleRepository.recupererRoleUser();
	}
	
	public Role roleParNom(String nom) {
		Role result = null;
		for (Role role : roleRepository.findAll()) {	
			if (role.getNom().equals(nom))
				result = role;
		}
		return result;	
	}
}
