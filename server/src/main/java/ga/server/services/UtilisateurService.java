package ga.server.services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ga.server.domaine.Login;
import ga.server.domaine.Role;
import ga.server.domaine.Site;
import ga.server.domaine.Utilisateur;
import ga.server.repository.LoginRepository;
import ga.server.repository.RoleRepository;
import ga.server.repository.SiteRepository;
import ga.server.repository.UtilisateurRepository;
import ga.server.services.dto.UtilisateurTO;

import java.util.EmptyStackException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

@Service
@Transactional
public class UtilisateurService implements UserDetailsService{
	@Autowired
	private UtilisateurRepository utilisateurRepo;
	@Autowired
	private LoginRepository loginRepo;
	@Autowired
	LoginService loginService;
	@Autowired
	RoleRepository roleRepo;
	@Autowired
	SiteRepository siteRepo;
	
	//Logger logger = LoggerFactory.getLogger(ProjetService.class);
	
	public Utilisateur creerUtilisateur(Utilisateur user, Set<Role> role, Set<Site> site) {

			Login login = new Login(user.getMail());
			loginRepo.save(login);
			user.setLogin(login);
		
		user.setRoles(role);
		user.setSites(site);
		utilisateurRepo.save(user);
		return user;
	}	

	
	public Optional<Utilisateur> getUtilisateur(Long id) {
		return utilisateurRepo.findById(id) ;
	}
	public Iterable<Utilisateur> listerUtilisateur() {
		return utilisateurRepo.listerUtilisateur();
	}
	
	public Utilisateur supprimerUtilisateur(String id) {
		long idd = Long.parseLong(id);
	
		Optional<Utilisateur> utilisateur = utilisateurRepo.findById(idd);
		if(utilisateur.isPresent()) {
			utilisateur.get().setActif(0);
			utilisateur.get().setLogin(null);
			utilisateurRepo.save(utilisateur.get());
			return utilisateur.get();
		}
		else {
			throw new EmptyStackException(); 
		}
	}
	
	public Utilisateur modifierUtilisateur(Utilisateur utilisateur) {
			return utilisateurRepo.save(utilisateur);
	}
	
	public Utilisateur recupererUtilisateurParID(Long id) {
		Optional<Utilisateur> utilisateur = utilisateurRepo.findById(id);
		
		if (utilisateur.isPresent()) {
			return utilisateur.get();
		}
		else {
			return new Utilisateur();
		}
	}
	
	public Utilisateur modifierUtilisateur(Utilisateur utilisateur, Long utilisateurId) {
			Optional<Utilisateur> getvalue = utilisateurRepo.findById(utilisateurId);
			if(getvalue.isPresent()) {
				Utilisateur utilisateurOptionnel = getvalue.get();
				utilisateurOptionnel.setNom(utilisateur.getNom());
				utilisateurOptionnel.setPrenom(utilisateur.getPrenom());
				utilisateurOptionnel.setMail(utilisateur.getMail());
				utilisateurOptionnel.setRoles(utilisateur.getRoles());
				utilisateurRepo.save(utilisateurOptionnel);
				return utilisateurOptionnel;
			}
			else {
				return null;
			}
			
	}
	
	public Utilisateur obtenirUtilisateurParId(Long id) {
		Optional<Utilisateur> userOptionnal = utilisateurRepo.findById(id);
		if (userOptionnal.isPresent()) {
			return userOptionnal.get();
		}
		return null;
	}
	
	

	@Override
	public UserDetails loadUserByUsername(String username) {
		try {
			return loginService.chercherParNom(username);
		}
		catch (UsernameNotFoundException e) {
			throw new UsernameNotFoundException("L'utilisateur n'a pas pu être récupéré", e);
		}
	}
	
	public List<Utilisateur> listerUtilisateurParRole(String role){
		return utilisateurRepo.findByRolesNom(role);
	}
}
