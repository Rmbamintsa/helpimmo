package ga.server.services.interfaces;

import java.util.List;

import ga.server.domaine.Site;
import ga.server.services.dto.SiteTO;

public interface ISiteService {

	/* CRUD BASIQUE */
	boolean creerSite(SiteTO siteTo);							//CREATE
	Site obtenirSiteParId(Long id);								//READ
	Site obtenirSiteParDesignation(String designation);			//READ par designation
	boolean modifierSite(SiteTO siteTo);						//UPDATE
	boolean supprimerSite(Long id);								//DELETE
	/* LISTING */
	List<Site> listerSites();
	
}