package ga.server.services.interfaces;

import org.springframework.security.core.userdetails.UserDetails;

import ga.server.domaine.Utilisateur;

public interface IUtilisateurService {
	
	Utilisateur creerUtilisateur(Utilisateur user);
	Utilisateur supprimerUtilisateur(String id);
	Utilisateur modifierUtilisateur(Utilisateur utilisateur, Long utilisateurId);
	UserDetails loadUserByUsername(String username);
	Iterable<Utilisateur> listerUtilisateur();

}
