package ga.server.services.interfaces;

import java.util.List;

import ga.server.domaine.Code;
import ga.server.domaine.Site;
import ga.server.services.dto.CodeTO;

public interface ICodeService {

	/* CRUD BASIQUE */
	boolean creerCode(CodeTO codeTo);						//CREATE
	Code obtenirCodeParId(Long id);							//READ par id
	Code obtenirCodeParDesignation(String designation);		//READ par designation
	boolean modifierCode(CodeTO codeTo);					//UPDATE
	//boolean supprimerCode(Long id);							//DELETE
	Code supprimerCode(Long id);
	void supprimerCodesInactifs(int id);
	/* LISTING */
	List<Code> listerCodes();
	List<Code> listerCodesParSite(Site site);
	
}