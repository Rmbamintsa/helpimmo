package ga.server.services.dto;

import java.util.Set;

import ga.server.domaine.Code;

public class SiteTO {

	private Long id;
	private String designation;
	private Set<Code> listeCode;
	
	public Long getId() {
		return id;
	}

	public Set<Code> getListeCode() {
		return listeCode;
	}

	public void setListeCode(Set<Code> listeCode) {
		this.listeCode = listeCode;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}


}