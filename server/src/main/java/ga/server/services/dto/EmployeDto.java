package ga.server.services.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeDto extends UtilisateurDto{
    private String code;
    private int nombreBienEncours;
    private int nombreBienTraite;
}
