package ga.server.services.dto;

public interface UtilisateurTO {

	Long getId();
	String getNom();
	String getPrenom();
	String getMail();
}
