package ga.server.services.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdministrateurDto extends UtilisateurDto{
    private String level;
    private String description;
}
