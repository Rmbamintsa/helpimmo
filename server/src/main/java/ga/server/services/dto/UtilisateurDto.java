package ga.server.services.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Calendar;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UtilisateurDto {
    private Long idUser;
    private String  name;
    private String prenom;
    private String email;
    private String password;
    private boolean actived;
    private Calendar dateCreation;
    private Calendar dateMAJ;
    private Collection<RoleDTO> roles;

}
