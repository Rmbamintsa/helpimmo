package ga.server.services.dto;

public class CodeTO {

	private Long id;
	private String designation;
	private Long  site;
	private int actif;
	
	public int isActif() {
		return actif;
	}

	public void setActif(int actif) {
		this.actif = actif;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public Long getSite() {
		return site;
	}
	public void setSite(Long site) {
		this.site = site;
	}
	
	
	
}
