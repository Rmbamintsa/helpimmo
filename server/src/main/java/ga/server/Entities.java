package ga.server;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Entities {
    public static final String UTILISATEUR = "utilisateur";
    public static final String API_PATH = "/api";
    public static final String ROLE = "role";

}
