package ga.server.domaine;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("SALARY")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employe extends Utilisateur {
private String code;
private int nombreBienEncours;
private int nombreBienTraite;


}
