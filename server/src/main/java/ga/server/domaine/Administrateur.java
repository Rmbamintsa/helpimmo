package ga.server.domaine;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Calendar;

@Entity
@DiscriminatorValue("INTERNE")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Administrateur extends Utilisateur {
    private String level;
    private String description;

}
