package ga.server.domaine;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ga.server.services.dto.SiteTO;

@Entity
@Table(name="T_SITE")
public class Site implements Serializable{

	private static final long serialVersionUID = -6499049281957253659L;


	/* ################################################################## */
	//							ATTRIBUTS
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id private Long id;
	
	@ManyToMany(mappedBy="sites",fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<Utilisateur> pums;
	
	@OneToMany(mappedBy = "site", cascade = { CascadeType.ALL }, orphanRemoval=true, fetch = FetchType.EAGER)
	private Set<Code> codes; 
	

	public Long getId() {
		return id;
	}

	private String designation;
	
	/* ################################################################## */
	//							CONSTRUCTEURS
	
	public Set<Code> getCodes() {
		return codes;
	}

	public void setCodes(Set<Code> codes) {
		this.codes = codes;
	}

	public Site() {
		
	}
	
	public Site(SiteTO siteTo) {
		
		this.designation = siteTo.getDesignation();
		
	}
	
	/* ################################################################## */
	//							ACCESSEURS
	
	public Set<Utilisateur> getPums() {
		return pums;
	}

	public void setPums(Set<Utilisateur> pums) {
		this.pums = pums;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}
	
	/* ################################################################## */
	
}