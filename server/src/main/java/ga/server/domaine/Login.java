package ga.server.domaine;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Entity
@Table(name="T_LOGIN")
public class Login implements UserDetails{
	private static final long serialVersionUID = -8286382391796976637L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	private String username;
	@Column
	private static final String PASSWORD = new BCryptPasswordEncoder().encode("") ;
	@OneToOne(targetEntity=Utilisateur.class, mappedBy = "login", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Utilisateur utilisateur;
	//
	public Login() {
		
	}
	public Login(String username) {
		this.username = username;
	}
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> authorities = new ArrayList<>();
		for (Role role: this.utilisateur.getRoles()) {	
			authorities.add(new SimpleGrantedAuthority(role.getNom()));
		}	
		return authorities;
	}
	
	@Override
	public String getPassword() {
		return PASSWORD;
	}
	@Override
	public String getUsername() {
		return username;
	}
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	@Override
	public boolean isEnabled() {
		return true;
	}
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
