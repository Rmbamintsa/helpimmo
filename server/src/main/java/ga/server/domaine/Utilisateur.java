package ga.server.domaine;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
//@NoArgsConstructor
//@AllArgsConstructor
@Entity
@Table(name = "T_UTILISATEUR")

public class Utilisateur implements Serializable {

	private static final long serialVersionUID = -1830775971561934538L;
	
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "nom")
    private String  nom;
    @Column(name = "prenom")
    private String prenom;
    @Column(name = "mail")
    private String mail;
    //@Column(name = "password")
    //private String password;
    @Column(name = "actif")
    private int actif;
    //@Column(name = "dateCreation")
    //private Calendar dateCreation;
    //@Column(name="dateMAJ")
    //private Calendar dateMAJ;
    @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
    private Set<Role> roles;
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonIgnore
	private Login login;	
	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE })
	private Set<Site> sites;
	
	public Set<Role> getRoles() {
		return roles;
	}
	
	public String getMail() {
		return mail;
	}

	public void setMail(String email) {
		this.mail = email;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public Set<Site> getSites() {
		return sites;
	}

	
	public void setSites(Set<Site> sites) {
		this.sites = new HashSet<>();
		this.sites = sites;
	}

	
	public void setRoles(Set<Role> role) {
		this.roles = new HashSet<>(role);
	}
	
	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}
	
	public int getActif() {
		return actif;
	}

	public void setActif(int actif) {
		this.actif = actif;
	}
	
	public Long getId() {
		return id;
	}

}
