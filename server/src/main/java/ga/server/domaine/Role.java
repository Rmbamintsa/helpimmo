package ga.server.domaine;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name="T_ROLE")
//@EqualsAndHashCode(of ={"code"} )
public class Role  { //implements Serializable
    @Id
    /*@GeneratedValue(strategy = GenerationType.AUTO,generator="native")
    @GenericGenerator(name ="native", strategy ="native")*/
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    //@Column(name = "code")
    //private String code;
    //@Column(name="description")
    //private String description;
    //@Column(name="dateCreation")
   //private Date dateCreation;
    
	@Column
	private String nom;
	@JsonIgnore
	@ManyToMany(mappedBy = "roles", fetch=FetchType.EAGER)
	private Collection<Utilisateur> users;
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	/*public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}*/
}
