package ga.server.domaine;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

import ga.server.services.dto.CodeTO;

@Entity
@Table(name="T_CODE")
public class Code implements Serializable {

	/* ################################################################## */
	//							ATTRIBUTS
	
	private static final long serialVersionUID = 244998415384699845L;

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id private Long id;
	
	@ManyToOne(optional=false, fetch = FetchType.EAGER)
	@JsonIgnore
	private Site site;
	
	private String designation;
	
	private int actif;
	
	public int isActif() {
		return actif;
	}

	public void setActif(int actif) {
		this.actif = actif;
	}
	
	/* ################################################################## */
	//							CONSTRUCTEURS
	
	public Code () {
		
	}
	
	public Code (CodeTO codeTo) {
		
		this.designation = codeTo.getDesignation();
	}
	
	/* ################################################################## */
	//							ACCESSEURS	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/* ################################################################## */
	
}
