package ga.server.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ga.server.domaine.Site;


@Repository
public interface SiteRepository extends CrudRepository<Site, Long> {

	@Query("from Site")
	public List<Site> listerSites();
	
	@Query("select count(site) = 0 from Site site where site.designation=:designation")
	public boolean sitePremierAjout(@Param("designation") String designation);
	
	@Query("from Site site where site.id=:id")
	public Site obtenirSiteParId(@Param("id") Long id);
	
	@Query("from Site site where site.designation=:designation")
	public Site obtenirSiteParDesignation(@Param("designation") String designation);
	
}