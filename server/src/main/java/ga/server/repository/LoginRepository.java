package ga.server.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ga.server.domaine.Login;

@Repository
public interface LoginRepository extends CrudRepository<Login, Long>{
	
	@Query("select l from Login l  where l.username = :username")
	Login findLoginByUsername(@Param("username") String username);
}
