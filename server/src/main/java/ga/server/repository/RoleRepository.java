package ga.server.repository;

import ga.server.domaine.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    //public Role findRoleByCode(String code);
	@Query("from Role r where r.id=1")
	public Role recupererRoleAdmin();
	
	@Query("from Role r where r.id=2")
	public Role recupererRoleCOMPT();
	
	@Query("from Role r where r.id=3")
	public Role recupererRoleCOM();
	
	@Query("from Role r where r.id=4")
	public Role recupererRoleUser();
}
