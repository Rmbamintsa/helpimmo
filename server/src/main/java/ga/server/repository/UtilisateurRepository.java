package ga.server.repository;

import ga.server.domaine.Utilisateur;
import ga.server.services.dto.UtilisateurTO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur,Long> {
	@Query("select u from Utilisateur u  where u.nom= :nom")
	List<Utilisateur> findByRolesNom(@Param("nom") String nom);
	@Query("select u from Utilisateur u  where u.actif=1 order by u.prenom")
	public Iterable<Utilisateur> listerUtilisateur();
}
