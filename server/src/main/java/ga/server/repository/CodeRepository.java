package ga.server.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ga.server.domaine.Code;
import ga.server.domaine.Site;

@Repository
public interface CodeRepository extends CrudRepository<Code, Long> {

	@Query("from Code")
	public List<Code> listerCodes();
	
	@Query("from Code code where code.site=:site")
	public List<Code> listerCodesParSite(@Param("site") Site site);
	
	@Query("select count(code) = 0 from Code code where code.designation=:designation")
	public boolean codePremierAjout(@Param("designation") String designation);
	
	@Query("from Code code where code.id=:id")
	public Code obtenirCodeParId(@Param("id") Long id);
	
	@Query("from Code code where code.designation=:designation")
	public Code obtenirCodeParDesignation(@Param("designation") String designation);
	
	@Transactional
	@Modifying
	@Query("delete from Code c where c.actif =:actif")
	public void supprimerCodesInactifs(@Param("actif") int actif);
	
}